#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>
#include <DHT_U.h>

#define MUESTRA 200

int pinDHT = 2;
float temperaturaDHT; // Temperatura dht
int f = 0;
float num = 261.0/5200.0;

unsigned short datos[MUESTRA]; 

int promedio(short datos[MUESTRA]){
  long suma = 0;
  for(int i = 0;i < MUESTRA;i++)
    suma += datos[i];

  return suma/MUESTRA;
}

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);
DHT dht(pinDHT, DHT22);

void setup() {
  Serial.begin(9600);

  for(int i = 0;i < MUESTRA;i++)
    datos[i] = 0;
/*
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.begin(16,2);
  lcd.clear();
*/
  dht.begin();
}

void loop() {
  datos[f] = analogRead(A0);
  f++;
  if(f > MUESTRA-1){
    f = 0;
    int promedioA0 = promedio(datos);
    //Serial.print("TERMOCUPLA");
    Serial.println(promedioA0);
    //Serial.print("DHT");
    //Serial.println(dht.readTemperature());
    /*
    lcd.clear();
    lcd.home();
    lcd.print("DHT: ");
    lcd.print(dht.readTemperature());
    lcd.print("°C");
    lcd.setCursor(0,1);
    lcd.print("Termpla: ");
    lcd.print();
    */
  }
  delay(1);
}
